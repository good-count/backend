from app.app import app
import unittest

class FlaskAppTest(unittest.TestCase):
	def setUp(self) -> None:
		self.client = app.test_client(self)
		return super().setUp()

	def test_error_index(self):
		response = self.client.get("/resource-that-does-not-exists")
		
		self.assertEqual(response.status_code, 404)

	def test_error_message(self):
		response = self.client.get("/resource-that-does-not-exists")
		
		self.assertEqual(
			response.json,
			{
				"message": "Page Not Found"
			}
		)

if __name__ == "__main__":
	unittest.main()