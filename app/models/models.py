from sqlalchemy import Table, Column, Integer, ForeignKey, create_engine, String
from sqlalchemy.orm import relation, relationship, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql.functions import user
from sqlalchemy.sql.sqltypes import DateTime
from app.base import Base


users_to_expenses = Table(
	"users_expenses", Base.metadata,
	Column("users_id", Integer, ForeignKey("users.id")),
	Column("expenses_id", Integer, ForeignKey("expenses.id"))
)

users_to_expense_group = Table(
	"users_to_expense_group", Base.metadata,
	Column("users_id", Integer, ForeignKey("users.id")),
	Column("expense_group_id", Integer, ForeignKey("expense_group.id"))
)


class Users(Base):
	__tablename__ = "users"

	id = Column(Integer, primary_key=True)
	name = Column(String)

	expenses = relationship("Expenses", secondary=users_to_expenses, back_populates="users")
	expense_group = relationship("ExpenseGroup", secondary=users_to_expense_group, back_populates="users")

class ExpenseGroup(Base):
	__tablename__ = "expense_group"

	id = Column(Integer, primary_key=True)
	name = Column(String)
	timestamp = Column(DateTime)

	users = relationship("Users", secondary=users_to_expense_group, back_populates="expense_group")
	expenses = relationship("Expenses")

class Expenses(Base):
	__tablename__ = "expenses"

	id = Column(Integer, primary_key=True)
	author = Column(Integer, ForeignKey("users.id"))
	price = Column(Integer)
	timestamp = Column(DateTime)

	expense_group_id = Column(Integer, ForeignKey("expense_group.id"))
	users = relationship("Users", secondary=users_to_expenses, back_populates="expenses")
