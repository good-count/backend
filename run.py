from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from app.app import app
from app.base import Base


def main():
	# Database
	engine = create_engine("sqlite:///db/database.sqlite3")
	Session = sessionmaker(engine)
	Base.metadata.create_all(engine)

	app.run(debug=True)


if __name__ == "__main__":
	main()